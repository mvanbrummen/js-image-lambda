var assert = require('assert');
const index = require('../index')

describe('index', function() {
  describe('parseRequest', function() {
    it('should parse request', function() {
        const event = {
            path: "/22cdhdhxjui6pa7l76n7zfbonu/resize;h=500;w=700;m=exact"
        };
        
        assert.notDeepStrictEqual(index.parseRequest(event.path),
        {
            assetId: "22cdhdhxjui6pa7l76n7zfbonu",
            height: 500,
            width: 700
        }
        
        );
    });
  });
});