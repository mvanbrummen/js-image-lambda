const AWS = require('aws-sdk');
const {
	gravity
} = require('sharp');
const s3 = new AWS.S3();
const fs = require('fs')

const sharp = require('sharp');

function parseRequest(path) {
	const pathParts = path.split("/");

	if (pathParts.length === 2) {
		return {
			assetId: pathParts[1]
		};
	}

	const m = {};
	pathParts[2].replace("resize;", "").split(";").map(i => {
		var a = i.split("=")
		m[a[0]] = a[1];
	});

	console.log("matrix params are " + JSON.stringify(m));

	return {
		assetId: pathParts[1],
		height: parseInt(m.h),
		width: parseInt(m.w),
		method: m.m,
		logo: m.logo,
		date: m.date
	};
}

async function applyEdits(image, requestMatrix) {
	var resize = {
		height: requestMatrix.height,
		width: requestMatrix.width,
	};
	if (requestMatrix.method === "exact") {
		resize.fit = 'fill';
	} else if (requestMatrix.method === "fit") {
		resize.fit = 'inside';
	} else if (requestMatrix.method === "limit") {
		resize.fit = 'inside';
		resize.withoutEnlargement = true;
	} else {
		resize = {};
	}

	var logo = [];
	if (requestMatrix.logo !== "false") {
		const canvasImage = await sharp(fs.readFileSync(process.cwd() + '/images/canvas-logo-for-scaling.png'))
			.resize({
				height: 30,
				width: 60
			})
			.toBuffer();

		logo.push({
			input: canvasImage,
			gravity: 'centre'
		});
	}

	// logo watermark
	if (requestMatrix.logo !== "false") {
		const logoImage = await sharp(fs.readFileSync(process.cwd() + '/images/logo.png'))
			.resize({
				height: 30,
				width: 60
			})
			.toBuffer();

		logo.push({
			input: logoImage,
			gravity: 'southeast'
		});
	}

	// date
	if (requestMatrix.date !== "false") {
		const dateImage = new Buffer(`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 14" width="32" height="14">
	<rect rx="3" ry="3" x="0" y="0" width="32" height="14" fill="#000" fill-opacity="0.7"/>
	<text x="50%" y ="50%" fill="#fff" dominant-baseline="middle" text-anchor="middle" style="font-family:'Arial'; font-size:9px;">Aug 13</text>
	</svg>`);

		logo.push({
			input: dateImage,
			gravity: 'southwest'
		})
	}

	const convertedImage = await sharp(image.Body)
		.resize(resize)
		.composite(logo)
		.toBuffer();

	return convertedImage;
}

exports.handler = async (event) => {

	try {
		console.log(event);

		const requestMatrix = parseRequest(event.path.toLowerCase());

		const originalImage = await s3.getObject({
			Bucket: process.env.SOURCE_BUCKET,
			Key: process.env.SOURCE_ROOT_PATH + "perm/" + requestMatrix.assetId
		}).promise();

		if (!event.path.includes("resize")) {
			return {
				statusCode: 200,
				isBase64Encoded: true,
				headers: {
					"Content-Type": "image/jpeg",
				},
				body: originalImage.Body.toString('base64')
			};
		}

		const convertedImage = await applyEdits(originalImage, requestMatrix);


		return {
			statusCode: 200,
			isBase64Encoded: true,
			headers: {
				"Content-Type": "image/jpeg",
			},
			body: convertedImage.toString('base64')
		};
	} catch (err) {
		console.log(err);

		if (err.status) {
			return {
				statusCode: err.status,
				isBase64Encoded: false,
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify(err)
			};
		} else {
			return {
				statusCode: 500,
				isBase64Encoded: false,
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					message: 'Internal error. Please contact the system administrator.',
					code: 'InternalError',
					status: 500
				})
			};
		}
	}

}